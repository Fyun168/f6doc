---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: F6软件下载
  # text: 软件下载中心 
  tagline: 一款车牌识别停车软件
  image: /Image/install.ico
  actions:
    - theme: brand
      text: 点击下载 最新版本
      link: http://www.fyun168.com:8092/newf6
    - theme: alt
      text: 下载 国际版
      link: http://download.fyun168.com/F6%E8%BD%AF%E4%BB%B6Mysql%E7%89%88%E6%9C%AC/F6Parking.Mysql.240303.exe

features:
  - title: 支持相机
    details: 海康，大华，臻识，华夏，芊熠，
  - title: 支持控制卡
    details: KF,FK,QW,QTCK,OLM,N8...
  - title: 支持语言
    details: 中文简体，中文繁体，英文，俄罗斯，柬埔寨，美国，新加坡...
---