import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "F6下载站",
  description: "F6高清车牌停车管理软件下载",
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    logo: '/Image/FyunCar.svg',
    nav: [
      { text: '主页', link: '/' },
      { text: '帮助中心', link: '/help/' },
      { text: '现场案例', link: '/markdown-examples' }
    ],

    sidebar: [
      {
        text: 'Examples',
        items: [
          { text: '软件安装教程', link: '/markdown-examples' },
          { text: '历史版本', link: '/markdown-examples' },
          { text: 'Markdown Examples', link: '/markdown-examples' },
          { text: 'Runtime API Examples', link: '/api-examples' }
        ]
      }
    ],

    footer: {

      copyright: '<a href="https://beian.miit.gov.cn/#/Integrated/index">粤ICP备2021141304号-1</a>',
      message: 'Copyright © 2022-present 停车系统研发中心 版权所有'
    }

    // footer: {
    //   copyright: '<a href="https://beian.miit.gov.cn/#/Integrated/index">粤ICP备2024164936号-2</a>',
    //   message: 'Copyright © 2022-present 深圳市擎纬智控科技有限公司 版权所有'
    // }
    
    
    // socialLinks: [
    //   { icon: 'github', link: 'https://github.com/vuejs/vitepress' }
    // ] 


  }
})
